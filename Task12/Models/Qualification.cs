﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12.Models
{
    public class Qualification
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public DateTime AwardedDate { get; set; }

        public ICollection<CoachQualification> CoachQualifications { get; set;  }

        public Qualification(string Name, string Category, DateTime AwardedDate)
        {
            this.Name = Name;
            this.Category = Category;
            this.AwardedDate = AwardedDate;
        }
    }
}
