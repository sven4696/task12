﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Task12.Models
{
    public class PreGameTradition
    {
        public int ID { get; set; }
        public string Sport { get; set; }
        public string Description { get; set; }
        // foreign key
        public int AthleteID { get; set; }
        // navigation property
        public Athlete Athlete { get; set; }

        public PreGameTradition(string Sport, string Description, int AthleteID)
        {
            this.Sport = Sport;
            this.Description = Description;
            this.AthleteID = AthleteID;
        }
    }
}
