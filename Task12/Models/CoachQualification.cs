﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Task12.Models
{
    public class CoachQualification
    {
        // foreign key for coach
        public int CoachID { get; set; }
        // navigation property
        public Coach Coach { get; set; }
        // foreign key for qualification
        public int QualificationID { get; set; }
        // navigation property
        public Qualification Qualification { get; set; }

        public CoachQualification(int CoachID, int QualificationID)
        {
            this.CoachID = CoachID;
            this.QualificationID = QualificationID;
        }

    }
}
