﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12.Models
{
    public class Athlete
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Dob { get; set; }
        public int awards { get; set; }

        //Forreign key 
        public int CoachId { get; set; }

        // Navigation property
        public Coach Coach { get; set; }

        // Navigation property
        public PreGameTradition PreGameTradition { get; set; }

        public Athlete (string Name, DateTime Dob, int awards, int CoachId)
        {
            this.Name = Name;
            this.Dob = Dob;
            this.awards = awards;
            this.CoachId = CoachId; 
        }
    }
}
