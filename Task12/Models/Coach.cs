﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12.Models
{
    public class Coach
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Dob { get; set; }
        public int ChampionshipWins { get; set; }

        // Relationship Coach has many athletes
        public ICollection<Athlete> Athletes { get; set; }

        // Relationship Coach can have many qualifications
        public ICollection<CoachQualification> coachQualifications { get; set; }

        public Coach(string Name, DateTime Dob, int ChampionshipWins)
        {
            this.Name = Name;
            this.Dob = Dob;
            this.ChampionshipWins = ChampionshipWins; 
        }
    }
}
