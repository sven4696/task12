﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Task12.Helpers;
using Task12.Models;


namespace Task12
{
    class Program
    {
        static void Main(string[] args)
        {

            SportsDBContext db = new SportsDBContext();

            // Deserialization
            Athlete ath = JSON.TakeFromJson<Athlete>("letSee");
            Console.WriteLine(ath.Coach.Athletes.Count);


            // Database method
            DatabaseMethods.AthletePregame(db, "Sven");
        }
    }
}
