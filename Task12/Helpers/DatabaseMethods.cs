﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Task12.Models;

namespace Task12.Helpers
{
    public class DatabaseMethods
    {
        // Deleting an Athlete in the database based on an index
        public static void DeleteAtheleteOnIndex(SportsDBContext db, int index)
        {
            var athlete = db.Athlete.Where(a => a.Id == index);

            db.Athlete.RemoveRange(athlete);
            db.SaveChanges();
        }

        // Inserting a coach to the Database
        public static void InsertAthlete(SportsDBContext db, Athlete athlete)
        {
            db.Add(athlete);
            db.SaveChanges();
        }

        // Adding a coach to the database
        public static void InsertCoach(SportsDBContext db, Coach coach)
        {
            db.Add(coach);
            db.SaveChanges();
        }

        // Findng coach and their qualification based on a given index
        public static void FindCoachAndQualificationIndex(SportsDBContext db, int index)
        {
            var info = db.CoachQualification.Include(cq => cq.Qualification).Include(co => co.Coach).Where( qua => qua.QualificationID == index).FirstOrDefault();

            Console.WriteLine($"{info.Coach.Name} has {info.Qualification.Category}");
        }

        // Finding athletes pregame tradition based on his name
        public static void AthletePregame(SportsDBContext db, string name)
        {
            var info = db.Athlete.Include(a => a.PreGameTradition).Where(a => a.Name == name).FirstOrDefault();

            Console.WriteLine($"The athlete {info.Name} which plays {info.PreGameTradition.Sport} has a special pregame tradition which involves {info.PreGameTradition.Description}");
        }

    }
}
