﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Task12.Models;
using Newtonsoft;
using System.IO;
using System.Collections.Generic;
namespace Task12.Helpers
{
    public class JSON
    {

        // write any object into a JSON file
        public static void WriteJsonFile<T>(string filename, List<T> list)
        {
            string path = $"C:\\Users\\sdanee\\source\\repos\\Task12\\Task12\\JsonFiles\\{filename}";
            string content = JsonConvert.SerializeObject(list[0], Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            File.WriteAllText(path, content);
        }

        // take out any object from a JSON file to a object
        public static T TakeFromJson<T>(string filename)
        {
            string path = @"C:\Users\sdanee\source\repos\Task12\Task12\JsonFiles\" + filename;
            string json = File.ReadAllText(path);

            T fileObject = JsonConvert.DeserializeObject<T>(json);
            return fileObject; 
        }



    }
}
