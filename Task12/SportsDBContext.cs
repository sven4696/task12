﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Task12.Models;

namespace Task12
{
    public class SportsDBContext : DbContext
    {
        public DbSet<Athlete> Athlete { get; set; }

        public DbSet<Coach> Coach { get; set; }

        public DbSet<CoachQualification> CoachQualification { get; set; }
        public DbSet<PreGameTradition> PreGameTradition { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        {
            optionsBuilder.UseSqlServer("Data Source= PC7267\\SQLEXPRESS; Initial Catalog= SportDB; Integrated Security= True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CoachQualification>().HasKey(cq => new { cq.CoachID, cq.QualificationID });
        }

   
    }
}
